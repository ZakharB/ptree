# Process Tree#

Provides a UI for displaying output similar to the **pstree -n -p** command

![Screenshot1](https://bytebucket.org/ZakharB/ptree/raw/374e886bcf896315d98ead00eb1647e47b552a91/screenshots/Screenshot%20from%202017-05-26%2008-12-09.png)


![Screenshot2](https://bytebucket.org/ZakharB/ptree/raw/374e886bcf896315d98ead00eb1647e47b552a91/screenshots/Screenshot%20from%202017-05-26%2008-12-38.png)

### Built with ###

* g++7.1
* Qt 5.8
* qbs 1.8
* c++17 with a filesystem support required