import QtQuick 2.7
import QtQuick.Controls 2.0
import "."

Control {
    id: root

    property bool showPid: false
    property bool highlight: hovered

    property string name
    property int pid
    property bool isTask

    hoverEnabled: false
    implicitWidth: background.implicitWidth
    implicitHeight: background.implicitHeight

    background: Rectangle {
        border.color: "yellow"
        border.width: highlight ? 2 : 0
        color: Style.colors.processBG
        implicitWidth: text.implicitWidth > 100 ? text.implicitWidth : 100
        implicitHeight: 25

        radius: implicitHeight / 2
        Text {
            id: text

            anchors.centerIn: parent
            text: (isTask ? "{%1}" : "%1").arg(
                      root.name + (root.showPid ? " (%1)".arg(root.pid) : ""))
        }
    }
}
