pragma Singleton
import QtQuick 2.7

QtObject {
    id: root

    property QtObject colors: QtObject {
        property color processBG: "#336B87"
        property color processDrawerBG: "#2A3132"
        property color topPanelBG: "#90AFC5"
    }
}
