import QtQuick 2.7
import QtQuick.Controls 2.0
import QtQuick.Layouts 1.0
import "."
import QtQml.Models 2.2
import QtQuick.Controls 1.4 as Old

ApplicationWindow {
    id: root
    visible: true
    width: 640
    height: 480
    title: qsTr("Process tree")

    header: Rectangle {
        color: Style.colors.topPanelBG
        height: 50

        Row {
            padding: 5
            anchors.verticalCenter: parent.verticalCenter
            TextField {
                id: pidTextFileld
                anchors.verticalCenter: parent.verticalCenter
                placeholderText: qsTr("Enter PID")
            }
            Button {
                text: "show"
                onPressed: {
                    viewModel.onShowPIDButtonPress(pidTextFileld.text)
                }
            }

            CheckBox {
                id: showPIDCheckBox
                text: "Show PID"
            }
        }
    }

    Rectangle {
        anchors.fill: parent

        color: Style.colors.processDrawerBG
        Flickable {
            id: flow
            anchors.fill: parent

            clip: true
            contentWidth: drawer.width
            contentHeight: drawer.height

            ProcessTreeVisualizer {
                id: drawer
                padding: 10
                model: viewModel.processModel
                showPid: showPIDCheckBox.checked
            }
        }
    }
}
