import QtQuick 2.7
import QtQml.Models 2.2

Column {
    id: root
    property alias model: visualModel.model
    property alias rootIndex: visualModel.rootIndex
    property int rowSpacing: 3
    property int colSpacing: 3

    property bool showPid: false

    function setRootIndex(index) {
        console.log(index)
        visualModel.rootIndex = index
    }

    spacing: colSpacing
    QtObject {
        id: internal
        property url nestedDrawer: ""
        property var index
    }

    Repeater {
        model: DelegateModel {
            id: visualModel

            delegate: Row {
                spacing: root.rowSpacing

                Process {
                    name: model.name
                    pid: model.pid
                    isTask: model.isTask
                    showPid: root.showPid
                }

                Loader {
                    id: recursiveLoader
                    source: internal.nestedDrawer
                    active: model.hasModelChildren
                    onLoaded: {
                        item.rootIndex = visualModel.modelIndex(index)
                        item.model = Qt.binding(function () {
                            return root.model
                        })
                        item.rowSpacing = Qt.binding(function () {
                            return root.rowSpacing
                        })
                        item.colSpacing = Qt.binding(function () {
                            return root.colSpacing
                        })
                        item.showPid = Qt.binding(function () {
                            return root.showPid
                        })
                    }
                }
            }
        }
    }

    // qml forbids recursive component createion, so we are using a loder to achive it
    // note the best way of doing stuff but the quickest one
    Component.onCompleted: {
        internal.nestedDrawer = "./ProcessTreeVisualizer.qml"
    }
}
