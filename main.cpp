//------------------------------------------------------------------------
// Copyright 2017 Physio-Control, Inc., All Rights Reserved
//
// This software is proprietary and confidential information belonging to
// Physio-Control, Inc., and may not be decompiled, disassembled, disclosed,
// reproduced or copied without the prior written consent of Physio-Control, Inc.
//
// %{Cpp:License:FileName}
//------------------------------------------------------------------------
#include <QDebug>
#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QQmlContext>

#include "app/ViewModel.h"

Q_DECLARE_METATYPE(ProcessInfo)

int main(int argc, char *argv[])
{
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
    QGuiApplication app(argc, argv);

    ViewModel vm;

    QQmlApplicationEngine engine;

    engine.rootContext()->setContextProperty("viewModel", &vm);
    engine.load(QUrl(QLatin1String("qrc:/main.qml")));

    return app.exec();
}
