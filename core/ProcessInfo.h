#ifndef PROCESSINFO_H
#define PROCESSINFO_H

#include <map>
#include <string>
#include <vector>

using pid_t = int;
struct ProcessInfo
{

    std::string        name;
    pid_t              pid;
    pid_t              pPid;
    pid_t              tGid;
    std::vector<pid_t> childPids;
    // a list of bot child processes (where the pPid is this processe) and taks(tGid references to this, but pPid is another process)
    // NOTE this requieres that the process info can't outlive its ProcessTree(where it comes from)
    std::vector<const ProcessInfo *> visualChildren;
};

using ProcessTree = std::map<pid_t, ProcessInfo>;

#endif  // PROCESSINFO_H
