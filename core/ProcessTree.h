#ifndef PROCESSTREE_H
#define PROCESSTREE_H

#include "ProcessInfo.h"

#include <iosfwd>
#include <optional>
#include <string_view>
#include <unordered_map>
#include <vector>

#include <experimental/filesystem>
namespace fs = std::experimental::filesystem;

class ProcessReader
{
public:
    ProcessReader();

    std::vector<pid_t> readExistingIDs() const;

    ProcessTree getAllProcesses();

private:
    bool readStat(const fs::path dirPath, ProcessInfo &processInfo);
    void fillChildren(ProcessTree &tree);

    static std::optional<pid_t> strToPid(const std::string_view &str);
};

#endif  // PROCESSTREE_H
