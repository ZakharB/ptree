#include <algorithm>
#include <fstream>
#include <iostream>
#include <regex>
#include <utility>

#include "ProcessTree.h"

using namespace std::string_literals;

static const auto       PROC_DIR_PATH = fs::path("/proc/");
static const std::regex digitsOnlyRegex("[0-9]*");

/*! The ProcessReader class reads all existing processes from the procfs*/
ProcessReader::ProcessReader()
{
}

/*!
 * Processes the whole tree.
 * Checks each process`s pPid and tGid.
 * If tGid of the process is equils its pid then
 * then this process pid shall be added to both parent's childPids and visualChildren.
 * If tGid and pid missmatch(this process is a task) then then it shall be added to parent's childPids,
 * but to visualChildren of the process with pid equil to tGid
 */
void ProcessReader::fillChildren(ProcessTree &tree)
{
    auto findProcess = [&tree](pid_t pid) -> ProcessInfo * {
        if (auto it = tree.find(pid); it != std::cend(tree))
        {
            return &it->second;  // pointer to element
        }
        else
        {
            return nullptr;
        }
    };
    for (auto & [ pid, proc ] : tree)
    {
        auto pPid = proc.pPid;

        if (auto parentProcPtr = findProcess(pPid); parentProcPtr)
        {
            if (proc.pid == proc.tGid)
            {
                parentProcPtr->childPids.push_back(pid);
                parentProcPtr->visualChildren.push_back(&proc);
            }
            else
            {
                parentProcPtr->childPids.push_back(pid);
                if (auto ownerPtr = findProcess(proc.tGid); ownerPtr)
                {
                    ownerPtr->visualChildren.push_back(&proc);
                }
            }
        }
        else
        {
            // process may end, ignore
        }
    }
}

/*! Converts string to pid_t*/
std::optional<pid_t> ProcessReader::strToPid(const std::string_view &str)
{
    // TODO replace with std::from_char, not available with g++7.1

    return std::stoi(std::string(str));
    auto it = std::begin(str);
    if (it == std::end(str))
    {
        return {};
    }
    pid_t result = 0;

    for (; it < std::end(str); ++it)
    {
        if (*it < '0' || *it > '9')
            return {};
        result *= 10;
        result += *it - '0';
    }
    return result;
}

/*! Fills processInfo with an informtaion from stat file.*/
bool ProcessReader::readStat(const fs::path dirPath, ProcessInfo &processInfo)
{
    // NOTE alternativly may be done with a scanf or regex parse
    std::ifstream statStream;
    auto          statPath = dirPath / "stat";
    statStream.open(statPath, std::ifstream::in);
    if (!statStream.is_open())
    {
        std::cerr << statPath << " not opened";  // TODO remove
        // process may end. ignore
        return false;
    }

    try
    {
        std::string data;
        std::getline(statStream, data);

        if (data.empty())
        {
            throw std::runtime_error("Corrupted stream, failed to read the ProcessInfo");
        }

        // pid
        auto dataView = std::string_view(data);
        {
            auto trimPos = dataView.find_first_of(' ');
            if (trimPos == dataView.npos)
            {
                throw std::runtime_error("Corrupted stream, failed to read the ProcessInfo");
            }
            auto pidView = dataView.substr(0, trimPos);
            if (auto converted = strToPid(pidView); converted.has_value())
            {
                processInfo.pid = converted.value();
            }
            else
            {
                throw std::runtime_error("Corrupted stream, failed to read the ProcessInfo");
            }

            dataView.remove_prefix(trimPos);
        }

        // name
        {
            auto nameBegin = dataView.find_first_of('(');
            auto nameEnd   = dataView.find_last_of(')');

            if (nameBegin == dataView.npos || nameEnd == dataView.npos)
            {
                throw std::runtime_error("Corrupted stream, failed to read the ProcessInfo");
            }
            processInfo.name = dataView.substr(nameBegin + 1, nameEnd - 2);

            dataView.remove_prefix(nameEnd + 1);
        }

        // state
        {
            // skip
            auto trimPos = dataView.find_first_of(' ', dataView.find_first_not_of(' '));
            if (trimPos == dataView.npos)
            {
                throw std::runtime_error("Corrupted stream, failed to read the ProcessInfo");
            }
            dataView.remove_prefix(trimPos);
        }

        // ppid
        {
            auto ppidBegin = dataView.find_first_not_of(' ');
            auto ppidEnd   = dataView.find_first_of(' ', ppidBegin);
            if (ppidBegin == dataView.npos || ppidEnd == dataView.npos)
            {
                throw std::runtime_error("Corrupted stream, failed to read the ProcessInfo");
            }
            auto ppidView = dataView.substr(ppidBegin, ppidEnd - 1);

            if (auto converted = strToPid(ppidView); converted.has_value())
            {
                processInfo.pPid = converted.value();
            }
            else
            {
                throw std::runtime_error("Corrupted stream, failed to read the ProcessInfo");
            }

            dataView.remove_prefix(ppidEnd);
        }
    }
    catch ([[maybe_unused]] std::exception &ex)
    {
        std::cerr << ex.what() << " with a proces " << statPath << '\n';

        // process may end. ignore
        return false;
    }
    return true;
}

/*! Returns existing PIDs*/
std::vector<pid_t> ProcessReader::readExistingIDs() const
{
    std::vector<pid_t> pids;
    std::regex         procRegex("/proc/[0-9]*");
    for (auto &p : fs::directory_iterator(PROC_DIR_PATH))
    {
        if (std::regex_match(std::string{p.path()}, procRegex) && fs::is_directory(p.status()))
        {
            std::string      path     = p.path();
            std::string_view pathView = path;
            auto             trim     = pathView.find_last_of('/');
            pathView.remove_prefix(trim + 1);
            if (auto res = strToPid(pathView); res.has_value())
            {
                pids.emplace_back(res.value());
            }
        }
    }
    return pids;
}

/*! Returns all existing processes */
ProcessTree ProcessReader::getAllProcesses()
{
    ProcessTree processes;
    // wheter the path ends ends with a directory from digits only
    auto isPathToDigitsDir = [](const fs::directory_entry &dirEntry) {
        return std::regex_match(std::string{dirEntry.path().filename()}, digitsOnlyRegex) && fs::is_directory(dirEntry.status());
    };
    for (auto &procPath : fs::directory_iterator(PROC_DIR_PATH))
    {
        if (isPathToDigitsDir(procPath))
        {
            if (ProcessInfo procInfo; readStat(procPath, procInfo))
            {
                processes[procInfo.pid]      = procInfo;
                processes[procInfo.pid].tGid = procInfo.pid;  // self

                // read a task folder now
                for (auto &taskPath : fs::directory_iterator(procPath / "task"))
                {
                    if (isPathToDigitsDir(taskPath))
                    {
                        if (ProcessInfo taskInfo; readStat(taskPath, taskInfo))
                        {
                            taskInfo.tGid           = procInfo.pid;  // parent;
                            processes[taskInfo.pid] = taskInfo;      // FIXME parent process is read twice
                        }
                    }
                }
            }
        }
        else
        {
            // process may end, ignore
        }
    }

    // every child add itseld as a child to the parent process
    fillChildren(processes);

    return processes;
}
