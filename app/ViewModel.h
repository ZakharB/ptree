#ifndef VIEWMODEL_H
#define VIEWMODEL_H

#include <QObject>

#include "ProcessTreeItem.h"
#include "ProcessTreeModel.h"
#include "core/ProcessTree.h"

class ViewModel : public QObject
{
    Q_OBJECT

    Q_PROPERTY(ProcessTreeModel *processModel READ processModel CONSTANT)
public:
    ViewModel();

    Q_INVOKABLE void onShowPIDButtonPress(int pid);

    ProcessTreeModel *processModel();

private:
    ProcessReader mCommand;
    ProcessTreeModel   mModel;
};

#endif  // VIEWMODEL_H
