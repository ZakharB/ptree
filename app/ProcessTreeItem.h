#ifndef PROCESSTREEITEM_H
#define PROCESSTREEITEM_H

#include <memory>

#include <QList>
#include <QString>
#include <QVariant>

#include "core/ProcessInfo.h"

class ProcessTreeItem
{
public:
    ProcessTreeItem(const ProcessInfo *data, ProcessTreeItem *parent);
    ~ProcessTreeItem();

    enum Fields
    {
        Pid,
        Name,
        IsTask,
    };

    int row() const;

    void appendChild(ProcessTreeItem *item);

    QVariant data(Fields field) const;

    ProcessTreeItem *child(int row);

    int childCount() const;

    ProcessTreeItem *parentItem();

private:
    const ProcessInfo *      mProccess;
    ProcessTreeItem *        mParentItem;
    QList<ProcessTreeItem *> mChildItems;
};

#endif  // PROCESSTREEITEM_H
