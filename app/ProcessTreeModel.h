#ifndef PROCESSTREEMODEL_H
#define PROCESSTREEMODEL_H

#include <QAbstractItemModel>

#include "ProcessTreeItem.h"
#include "core/ProcessInfo.h"

class ProcessTreeModel : public QAbstractItemModel
{
    Q_OBJECT

public:
    enum ProcessRoles
    {
        NameRole = Qt::UserRole + 1,
        PidRole,
        IsTask,
    };

    explicit ProcessTreeModel(QObject *parent = 0);
    QVariant data(const QModelIndex &index, int role) const override;
    QModelIndex index(int row, int column, const QModelIndex &parent = QModelIndex()) const override;
    QModelIndex parent(const QModelIndex &index) const override;
    int rowCount(const QModelIndex &parent = QModelIndex()) const override;
    int columnCount(const QModelIndex &parent = QModelIndex()) const override;

    QHash<int, QByteArray> roleNames() const override;

    void setProcesses(ProcessTree tree, pid_t rootPID);

private:
    void convertImp(const ProcessTree &tree, const ProcessInfo &rootProcess, ProcessTreeItem *parent);

    ProcessTree                      mProcessTree;
    std::unique_ptr<ProcessTreeItem> mRootItem{nullptr};
};

#endif  // PROCESSTREEMODEL_H
