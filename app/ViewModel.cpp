#include "ViewModel.h"

/*!
 * Is used for user interaction
 */
ViewModel::ViewModel()
{
}

/*! Rereads all processes and puts them to model to build a tree for specified pid*/
void ViewModel::onShowPIDButtonPress(int pid)
{
    mModel.setProcesses(mCommand.getAllProcesses(), pid);
}

ProcessTreeModel *ViewModel::processModel()
{
    return &mModel;
}
