#include "ProcessTreeItem.h"

/*!
 * Wrapper around the ProcessInfo.
 * Used in the ProcessTreeModel class, to simplify managing.
 * This wrapper can be ommited model can work dirrectly with the ProcessTree,
 * but I have no time for it
*/
ProcessTreeItem::ProcessTreeItem(const ProcessInfo *data, ProcessTreeItem *parent) : mProccess{data}, mParentItem{parent}
{
}

ProcessTreeItem::~ProcessTreeItem()
{
    qDeleteAll(mChildItems);
}

/*! Reports the item's location within its parent's list of items */
int ProcessTreeItem::row() const
{
    if (mParentItem)
        return mParentItem->mChildItems.indexOf(const_cast<ProcessTreeItem *>(this));

    return 0;
}

/*! Adds child item */
void ProcessTreeItem::appendChild(ProcessTreeItem *item)
{
    mChildItems << item;
}

QVariant ProcessTreeItem::data(Fields field) const
{
    // clang-format off
    switch (field)
    {
        case Fields::Pid:    return mProccess->pid;
        case Fields::Name:   return mProccess->name.c_str();  // implicit cast to qstring
        case Fields::IsTask: return mProccess->pid != mProccess->tGid;  // implicit cast to qstring
        default: return {};
    }
    // clang-format on
}

/*! Returns the child at a given position*/
ProcessTreeItem *ProcessTreeItem::child(int row)
{
    return mChildItems.value(row);
}

/*! The count of children */
int ProcessTreeItem::childCount() const
{
    return mChildItems.count();
}

/*! Pointer to the parent item*/
ProcessTreeItem *ProcessTreeItem::parentItem()
{
    return mParentItem;
}
