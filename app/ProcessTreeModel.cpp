#include "ProcessTreeModel.h"


/*! Model that is used to present the Process tree on screen*/
ProcessTreeModel::ProcessTreeModel(QObject *parent) : QAbstractItemModel(parent)
{
}

/*! \reimp */
QVariant ProcessTreeModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid())
        return QVariant();

    auto *item = static_cast<ProcessTreeItem *>(index.internalPointer());

    // clang-foemat off
    switch (role)
    {
        case NameRole: return item->data(ProcessTreeItem::Name);
        case PidRole:  return item->data(ProcessTreeItem::Pid);
        case IsTask:   return item->data(ProcessTreeItem::IsTask);
        default:       return QVariant();
    }
    // clang-foemat on
}

/*! \reimp */
QModelIndex ProcessTreeModel::index(int row, int column, const QModelIndex &parent) const
{
    if (!hasIndex(row, column, parent))
        return QModelIndex();

    auto parentItem = parent.isValid() ? static_cast<ProcessTreeItem *>(parent.internalPointer()) : mRootItem.get();
    auto childItem = parentItem->child(row);

    return childItem ? createIndex(row, column, childItem) : QModelIndex();
}

/*! \reimp */
QModelIndex ProcessTreeModel::parent(const QModelIndex &index) const
{
    if (!index.isValid())
        return QModelIndex();

    auto *childItem  = static_cast<ProcessTreeItem *>(index.internalPointer());
    auto *parentItem = childItem->parentItem();

    if (parentItem == mRootItem.get())
        return QModelIndex();

    return createIndex(parentItem->row(), 0, parentItem);
}

/*! The number of child items for the TreeItem that corresponds to a given model index,
 * or the number of top-level items if an invalid index is specifie
 */
int ProcessTreeModel::rowCount(const QModelIndex &parent) const
{
    if (parent.column() > 0 || !mRootItem)
        return 0;

    auto parentItem = parent.isValid() ? static_cast<ProcessTreeItem *>(parent.internalPointer()) : mRootItem.get();
    return parentItem->childCount();
}

/*! The number of fields */
int ProcessTreeModel::columnCount([[maybe_unused]]const QModelIndex &parent) const
{
    return 3;
}

/*! Roles for access from QML */
QHash<int, QByteArray> ProcessTreeModel::roleNames() const
{
    QHash<int, QByteArray> roles;
    roles[NameRole] = "name";
    roles[PidRole]  = "pid";
    roles[IsTask]   = "isTask";
    return roles;
}

/*! Sets the process tree where the information cames from and pid for which to build a tree*/
void ProcessTreeModel::setProcesses(ProcessTree tree, pid_t rootPID)
{
    beginResetModel();
    mProcessTree = std::move(tree);

    mRootItem.reset(new ProcessTreeItem(nullptr, nullptr));
    if (auto it = mProcessTree.find(rootPID); it != std::end(mProcessTree))
    {
        convertImp(mProcessTree, it->second, mRootItem.get());
    }
    endResetModel();
}

/*! Converts ProcessTree to ProcessTreeItem. This stem is unnecessary but I have not time to change it*/
void ProcessTreeModel::convertImp(const ProcessTree &tree, const ProcessInfo &rootProcess, ProcessTreeItem *parent)
{
    auto procRootItem = new ProcessTreeItem(&rootProcess, parent);
    parent->appendChild(procRootItem);

    for (auto child : rootProcess.visualChildren)
    {
        if (child)
        {
            convertImp(tree, *child, procRootItem);
        }
    }
}
