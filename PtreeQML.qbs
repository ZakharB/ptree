import qbs

Project {
    Product {
        name: "ProcessTreeQML"
        type: "application"

        consoleApplication: false
        Depends {
            name: "cpp"
        }
        Depends {
            name: "Qt"
            submodules: ["core", "qml", "quick"]
        }

        cpp.includePaths: ["."]
        cpp.cxxLanguageVersion: "c++1z"
        cpp.staticLibraries: ["stdc++fs"]

        Group {
            name: "files"
            prefix: "./**/"
            files: ["*.cpp", "*.h", "*.qrc"]
        }

        Group {
            name: "qml"
            prefix: "./**/"
            files: ["*.qml", "qmldir"]
        }
    }
}
